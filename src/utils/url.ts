import { useMemo } from "react";
import { URLSearchParamsInit, useSearchParams } from "react-router-dom";
import { cleanObject } from "utils";

/**
 * @param keys
 * @returns 返回页面URL中参数键值对，与改变参数的方法setSearchParam
 * searchParams 是URLSearchParams的一个对象，是一个iterator接口，有Symbol(Symbol.iterator)方法，可以用for...of遍历
 * setSearchParams 用来改变url上的参数值
 * Object.fromEntries方法把键值对列表转换为一个对象
 */
export const useUrlQueryParam = <K extends string>(keys: K[]) => {
  const [searchParams, setSearchParam] = useSearchParams();
  // 将数组转换成键值对
  return [
    // prev: [x: string]: string; searchParams.get(next)返回的类型是string|null
    // 所以对key赋值的时要判断null并置''
    // reduce这里返回的类型是{}，但是不符合真正的类型，需要手动将它变成
    // { [key in string]: string }
    // 使用useMemo防止无限循环
    useMemo(
      () =>
        keys.reduce((prev, key) => {
          return {
            ...prev,
            [key]: searchParams.get(key) || "",
          };
        }, {} as { [key in K]: string }),
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [searchParams]
    ),
    (params: Partial<{ [key in K]: unknown }>) => {
      const o = cleanObject({
        ...Object.fromEntries(searchParams),
        ...params,
      }) as URLSearchParamsInit;
      return setSearchParam(o);
    },
    // 对于tuple类型，ts会将里面的元素类型统一化，所以当出现不同类型时就会用|来涵盖所有类型
    // 这样显得类型很乱，通常对于这种问题可以在后面添加 as const解决，添加之后的类型就时特定的了
    //类型断言,值 as 类型 或<类型>值的形式，as const 是把对象的属性都设置为const，即只读的readonly
  ] as const;
};

/**
 * https://codesandbox.io/s/keen-wave-tlz9s?file=/src/App.js
 * export default function App() {
  // 当obj是基本类型的时候，就不会无限循环
  // 当 obj是对象的时候，就会无限循环
  // 当 obj 是对象的state时，不会无限循环
  const [obj, setObj] = useState({ name: "Jack" });
  // const obj = 1;
  // const obj = {name: 'Jack'}
  const [num, setNum] = useState(0);

  useEffect(() => {
    console.log("effect");
    setNum(num + 1);
  }, [obj]);

  return (
    <div className="App">
      {num}
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
    </div>
  );
}
 */
