import { useState, useEffect, useRef } from "react";

// object类型可能是{键值对}、函数、数组等多种类型
//@ts-ignore 用来忽略ts检查
const isFalsy = (value: any) => (value === 0 ? false : !value);
export const cleanObject = (obj: { [key: string]: unknown }) => {
  const result = { ...obj };
  Object.keys(result).forEach((key) => {
    const value = result[key];
    if (isFalsy(value)) {
      delete result[key];
    }
  });
  return result;
};

export const useDebounce = <V>(value: V, delay?: number) => {
  const [debounceValue, setDebounceVal] = useState(value);

  useEffect(() => {
    const timeout = setTimeout(() => setDebounceVal(value), delay);
    return () => clearTimeout(timeout);
  }, [value, delay]);
  return debounceValue;
};

export const useMount = (callback: () => void) => {
  useEffect(() => {
    callback();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
};

export const useDocumentTitle = (
  title: string,
  keepOnUnmount: boolean = true
) => {
  //useRef .current在整个生命中期中值不变, 下一个周期会改变
  const oldTitle = useRef(document.title).current;

  useEffect(() => {
    document.title = title;
  }, [title]);

  useEffect(() => {
    return () => {
      if (!keepOnUnmount) {
        //进入新的页面，如果没有指定title，读到的就是旧title
        document.title = oldTitle;
      }
    };
  }, [oldTitle, keepOnUnmount]);
};

export const resetRoute = () => {
  window.location.href = window.location.origin;
};

export const useMountedRef = () => {
  const mountedRef = useRef(false);
  useEffect(() => {
    // 页面初次渲染时设置为true
    mountedRef.current = true;
    // 页面卸载前设置为false
    return () => {
      mountedRef.current = false;
    };
  });
  return mountedRef;
};
