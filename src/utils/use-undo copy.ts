import { useState } from "react";

export const useUndo = <T>(initPresent: T) => {
  //这里要用泛型，否则past会是never
  const [past, setPast] = useState<T[]>([]);
  const [present, setPresent] = useState(initPresent);
  const [future, setFuture] = useState<T[]>([]);

  const canUndo = past.length !== 0;
  const canRedo = future.length !== 0;

  const undo = () => {
    if (!canUndo) return;

    const previous = past[past.length - 1];
    const newPast = past.slice(0, past.length - 1);

    setPast(newPast);
    setPresent(previous);
    setFuture([present, ...future]);
  };
  const redo = () => {
    if (!canRedo) return;

    const next = future[0];
    const newFuture = future.slice(1);

    setPast([...past, present]);
    setPresent(next);
    setFuture(newFuture);
  };
  const set = (newPresent: T) => {
    if (present === newPresent) return;

    setPast([...past, newPresent]);
    setPresent(newPresent);
    setFuture([]);
  };
  const reset = (newPresent: T) => {
    setPast([]);
    setPresent(newPresent);
    setFuture([]);
  };
  return [
    { past, present, future },
    { undo, redo, set, reset, canUndo, canRedo },
  ] as const;
};

// undo例子链接： https://codesandbox.io/s/confident-fast-ki7n0?file=/src/App.js
/**
export default function App() {
  const [
    countState,
    {
      set: setCount,
      reset: resetCount,
      undo: undoCount,
      redo: redoCount,
      canUndo,
      canRedo
    }
  ] = useUndo(0);

  const { present: presentCount } = countState;
  return (
    <div className="App">
      <p>you clicked {presentCount} times</p>
      <button key="increment" onClick={() => setCount(presentCount + 1)}>
        +
      </button>
      <button key="decrement" onClick={() => setCount(presentCount - 1)}>
        -
      </button>
      <button key="undo" onClick={undoCount} disabled={!canUndo}>
        undo
      </button>
      <button key="redo" onClick={redoCount} disabled={!canRedo}>
        redo
      </button>
      <button key="reset" onClick={() => resetCount(0)}>
        reset
      </button>
    </div>
  );
}
 */
