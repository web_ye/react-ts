import { useCallback, useReducer, useState } from "react";
import { useMountedRef } from "utils";

interface State<D> {
  error: Error | null;
  data: D | null;
  stat: "idle" | "loading" | "success" | "error";
}

const defaultInitState: State<null> = {
  stat: "idle",
  data: null,
  error: null,
};

//用来派发任务，但不是直接派发，只有当前不是卸载状态时才可以派发
const useSafeDispatch = <T>(dispatch: (...args: T[]) => void) => {
  const mountedRef = useMountedRef();
  return useCallback(
    (...args: T[]) => {
      mountedRef.current ? dispatch(...args) : void 0;
    },
    [dispatch, mountedRef]
  );
};

export const useAsync = <D>(initState?: State<D>) => {
  // const [state, setState] = useState({
  //   ...defaultInitState,
  //   ...initState,
  // });

  const [state, dispatch] = useReducer(
    (state: State<D>, action: Partial<State<D>>) => ({ ...state, ...action }),
    {
      ...defaultInitState,
      ...initState,
    }
  );

  const safeDispatch = useSafeDispatch(dispatch);

  // useState直接传入函数的含义：惰性初始化，如果想要用useState保存函数，不能直接传入函数，可以传入一个函数内部return一个函数
  // https://codesandbox.io/s/blissful-water-230u4?file=/src/App.js
  const [retry, setRetry] = useState(() => () => {});
  const setData = useCallback(
    (data: D) =>
      safeDispatch({
        data,
        stat: "success",
        error: null,
      }),
    [safeDispatch]
  );

  const setError = useCallback(
    (error: Error) =>
      safeDispatch({
        error,
        stat: "error",
        data: null,
      }),
    [safeDispatch]
  );
  //用来触发异步请求
  const run = useCallback(
    (promise: Promise<D>, runConfig?: { retry: () => Promise<D> }) => {
      if (!promise || !promise.then) {
        throw new Error("请输入Promise类型数据");
      }
      safeDispatch({ stat: "loading" });
      // setState((prevState) => ({ ...prevState, stat: "loading" }));
      // retry被调用时执行run这样可以更新state，最后可以刷新页面
      setRetry(() => () => {
        if (runConfig?.retry) {
          run(runConfig?.retry(), runConfig);
        }
      });
      return promise
        .then((data) => {
          setData(data);
          return data;
        })
        .catch((error) => {
          setError(error);
          return error;
        });
    },
    [setData, setError, safeDispatch]
  );
  return {
    isIdle: state.stat === "idle",
    isLoading: state.stat === "loading",
    isError: state.stat === "error",
    isSuccess: state.stat === "success",
    run,
    retry,
    setData,
    setError,
    ...state,
  };
};

// export default function App() {
//     const callbackRef = React.useRef(() => alert("init"));
//     const callback = callbackRef.current;
//     console.log(callback);
//     return (
//       <div className="App">
//         <button onClick={() => (callbackRef.current = () => alert("updated"))}>
//           setCallback
//         </button>
// 不会更新
//         <button onClick={callback}>call callback</button>
// 可以更新
// <button onClick={callbackRef.current()}>call callback</button>
//         <h1>Hello CodeSandbox</h1>
//         <h2>Start editing to see some magic happen!</h2>
//       </div>
//     );
//   }
