import { useCallback, useReducer } from "react";

// useReducer与useState的区别，useReducer适合多个有关联的状态的控制
const UNDO = "UNDO";
const REDO = "REDU";
const SET = "SET";
const RESET = "RESET";

type State<T> = {
  past: T[];
  present: T;
  future: T[];
};
type Action<T> = {
  newPresent?: T;
  type: typeof UNDO | typeof REDO | typeof SET | typeof RESET;
};

const undoReducer = <T>(state: State<T>, action: Action<T>) => {
  const { past, present, future } = state;
  const { newPresent, type } = action;

  switch (type) {
    case UNDO: {
      if (!past.length) return state;
      const previous = past[past.length - 1];
      const newPast = past.slice(0, past.length - 1);

      return {
        past: newPast,
        present: previous,
        future: [present, ...future],
      };
    }
    case REDO: {
      if (!future.length) return state;

      const next = future[0];
      const newFuture = future.slice(1);
      return {
        past: [...past, present],
        present: next,
        future: newFuture,
      };
    }
    case SET: {
      if (present === newPresent) return state;
      return {
        past: [...past, newPresent],
        present: newPresent,
        future: [],
      };
    }
    case RESET: {
      return {
        past: [],
        present: newPresent,
        future: [],
      };
    }
  }
};

export const useUndo = <T>(initPresent: T) => {
  const [state, dispatch] = useReducer(undoReducer, {
    past: [],
    present: initPresent,
    future: [],
  } as State<T>);

  const canUndo = state.past.length !== 0;
  const canRedo = state.future.length !== 0;

  const undo = useCallback(() => dispatch({ type: UNDO }), []);
  const redo = useCallback(() => dispatch({ type: REDO }), []);
  const set = useCallback(() => dispatch({ type: SET }), []);
  const reset = useCallback(() => dispatch({ type: RESET }), []);

  return [state, { undo, redo, set, reset, canUndo, canRedo }] as const;
};

// 普通hook的写法
// export const useUndo = <T>(initPresent: T) => {
//   //这里要用泛型，否则past会是never
//   const [state, setState] = useState<{
//     past: T[];
//     present: T;
//     future: T[];
//   }>({
//     past: [],
//     present: initPresent,
//     future: [],
//   });

//   const canUndo = state.past.length !== 0;
//   const canRedo = state.future.length !== 0;

//   const undo = useCallback(() => {
//     setState((currentState) => {
//       const { past, present, future } = currentState;
//       if (!past.length) return currentState;
//       const previous = past[past.length - 1];
//       const newPast = past.slice(0, past.length - 1);

//       return {
//         past: newPast,
//         present: previous,
//         future: [present, ...future],
//       };
//     });
//   }, []);

//   const redo = useCallback(() => {
//     setState((currentState) => {
//       const { past, present, future } = currentState;
//       if (!future.length) return currentState;

//       const next = future[0];
//       const newFuture = future.slice(1);
//       return {
//         past: [...past, present],
//         present: next,
//         future: newFuture,
//       };
//     });
//   }, []);

//   const set = useCallback((newPresent: T) => {
//     setState((currentState) => {
//       const { past, present } = currentState;
//       if (present === newPresent) return currentState;
//       return {
//         past: [...past, newPresent],
//         present: newPresent,
//         future: [],
//       };
//     });
//   }, []);

//   const reset = useCallback((newPresent: T) => {
//     setState((currentState) => {
//       return {
//         past: [],
//         present: newPresent,
//         future: [],
//       };
//     });
//   }, []);

//   return [state, { undo, redo, set, reset, canUndo, canRedo }] as const;
// };
