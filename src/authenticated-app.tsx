import React, { useState } from "react";
import { ProjectListScreen } from "screens/project-list";
import { ProjectDetail } from "screens/project";
import { useAuth } from "context/auth-context";
import styled from "@emotion/styled";
import { Button, Dropdown, Menu } from "antd";
import { ReactComponent as SoftwareLogo } from "assets/software-logo.svg";
import { Row, ButtonNoPadding } from "components/lib";
import { Routes, Route, Navigate } from "react-router";
import { BrowserRouter as Router } from "react-router-dom";
import { resetRoute } from "utils";
import { ProjectModel } from "components/project-model";
import { ProjectPopover } from "components/project-popover";

export const AuthenticatedApp = () => {
  const [projectModelOpen, setProjectModelOpen] = useState(false);

  return (
    <Container>
      <PageHeader
        projectButton={
          <ButtonNoPadding
            type={"link"}
            onClick={() => setProjectModelOpen(true)}
          >
            创建项目
          </ButtonNoPadding>
        }
      />
      <Main>
        <Router>
          <Routes>
            <Route
              path={"/projects"}
              element={
                <ProjectListScreen open={() => setProjectModelOpen(true)} />
              }
            ></Route>
            <Route
              path={"/projects/:projectId/*"}
              element={<ProjectDetail />}
            ></Route>
            {/* //匹配不到以上路由时匹配这里的路由 */}
            <Navigate to={"/projects"} />
          </Routes>
        </Router>
      </Main>
      <ProjectModel
        projectModelOpen={projectModelOpen}
        onClose={() => setProjectModelOpen(false)}
      />
    </Container>
  );
};
const PageHeader = (props: { projectButton: JSX.Element }) => {
  const { logout, user } = useAuth();

  const menu = (
    <Menu>
      <Menu.Item>
        <Button onClick={logout}>登出</Button>
      </Menu.Item>
    </Menu>
  );

  return (
    <Header between={true}>
      <HeaderLeft gap={true}>
        <Button type={"link"} onClick={resetRoute}>
          <SoftwareLogo width={"18rem"} color={"rgb(38, 132, 255)"} />
        </Button>
        <ProjectPopover projectButton={props.projectButton} />
        <span>组员</span>
      </HeaderLeft>
      <HeaderRight>
        <Dropdown overlay={menu}>
          <Button type="link" onClick={(e) => e.preventDefault()}>
            Hi, {user?.name}
          </Button>
        </Dropdown>
      </HeaderRight>
    </Header>
  );
};

const Container = styled.div`
  display: grid;
`;
const Header = styled(Row)`
  padding: 3.2rem;
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.1);
  z-index: 1;
`;
const HeaderLeft = styled(Row)``;

const HeaderRight = styled.div``;
const Main = styled.div`
  padding: 3.2rem;
  width: 100%;
  display: flex;
  flex-direction: column;
`;
