import { Drawer } from "antd";

export const ProjectModel = (props: {
  projectModelOpen: boolean;
  onClose: () => void;
}) => {
  return (
    <Drawer
      width={"100%"}
      title="Project Model"
      onClose={props.onClose}
      visible={props.projectModelOpen}
    ></Drawer>
  );
};
