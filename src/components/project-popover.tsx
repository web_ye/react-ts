import { Popover, List, Divider, Typography } from "antd";
import { useProjects } from "utils/project";
import styled from "@emotion/styled";

export const ProjectPopover = (props: { projectButton: JSX.Element }) => {
  const { Text } = Typography;
  const { data: projects } = useProjects();
  const pinedProjects = projects?.filter((item) => item.pin);
  const content = (
    <ContenContainer>
      <Text type={"secondary"}>收藏项目</Text>
      <List
        dataSource={pinedProjects}
        renderItem={(item) => <List.Item>{item.name}</List.Item>}
      />
      <Divider />
      {props.projectButton}
    </ContenContainer>
  );
  return (
    <Popover content={content}>
      <span>项目</span>
    </Popover>
  );
};

const ContenContainer = styled.div`
  min-width: 30rem;
`;
