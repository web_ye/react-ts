import { Select } from "antd";
import { Row } from "type";

//值透传,对于select其他属性可以直接传进来
type SelectProps = React.ComponentProps<typeof Select>;
//value，options，onChange是select的原有属性，这样写可以确保被IdSelectProps的对应属性覆盖
//如果不用Omit去除，结果会被合并
interface IdSelectProps
  extends Omit<SelectProps, "value" | "options" | "onChange"> {
  value: Row | null | undefined;
  onChange: (value?: number) => void;
  defaultOptionName?: string;
  options?: { name: string; id: number }[];
}
const toNumber = (value: unknown) => (isNaN(Number(value)) ? 0 : Number(value));

/**
 * @param props
 * @returns
 * value 可以传入多种类型的值
 * onChange只会接收number类型或undefined类型
 * 当isNaN(Number(value)为true会选择默认类型
 * 当选择默认类型是，onChange会调用undefined
 */

export const IdSelect = (props: IdSelectProps) => {
  // ...restProps表示其余的属性
  const { value, onChange, defaultOptionName, options, ...restProps } = props;
  return (
    <Select
      value={options?.length ? toNumber(value) : 0}
      onChange={(value) => onChange(toNumber(value) || undefined)}
      {...restProps}
    >
      {defaultOptionName ? (
        <Select.Option value={0}>{defaultOptionName}</Select.Option>
      ) : null}
      {options?.map((item) => (
        <Select.Option key={item.id} value={item.id}>
          {item.name}
        </Select.Option>
      ))}
    </Select>
  );
};
