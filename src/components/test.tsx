import { useEffect, useState } from "react";

//react hook 与闭包的问题
export const Test = () => {
  const [num, setNum] = useState(0);
  const add = () => setNum(num + 1);

  useEffect(() => {
    //每次const重新定义一个setInterval才能取到最新num值，否则num一直未0
    const intervel = setInterval(() => {
      console.log("interval: ", num);
    }, 2000);

    return () => clearInterval(intervel);
  }, [num]);

  //参数要传[num]每次num变化的时候才会调用useEffect，卸载的时候num才是最新值；
  //否则只在初始化时调用一次useEffect,导致卸载的时候num还是为0；
  useEffect(() => {
    return () => {
      console.log("卸载值:", num);
    };
  }, [num]);

  return (
    <div>
      <button onClick={add}>add</button>
      <p>{num}</p>
    </div>
  );
};
