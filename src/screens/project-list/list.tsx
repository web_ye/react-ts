import { Table, TableProps, Menu, Dropdown } from "antd";
import dayjs from "dayjs";
import { Project, User } from "./index";
import { Link } from "react-router-dom";
import { Pin } from "components/pin";
import { useEditProject } from "utils/project";
import { ButtonNoPadding } from "components/lib";

interface ListProps extends TableProps<Project> {
  users: User[];
  refresh: () => void;
  open: () => void;
}
export const List = ({ users, refresh, ...props }: ListProps) => {
  const { mutate } = useEditProject();
  // const pinProject = (id: number, pin: boolean) => mutate({id, pin});
  //柯里化写法
  const pinProject = (id: number) => (pin: boolean) =>
    mutate({ id, pin }).then(refresh);
  return (
    <Table
      rowKey="id"
      pagination={false}
      columns={[
        {
          title: <Pin checked={true} disabled={true} />,
          render: (value, project) => {
            // return <Pin checked={project.pin} onCheckedChange={pin => pinProject(project.id, project.pin)}/>
            return (
              <Pin
                checked={project.pin}
                onCheckedChange={pinProject(project.id)}
              />
            );
          },
        },
        {
          title: "名称",
          dataIndex: "name",
          sorter: (a, b) => a.name.localeCompare(b.name),
          render: (value, project) => {
            return <Link to={`${project.id}`}>{project.name}</Link>;
          },
        },
        {
          title: "部门",
          dataIndex: "organization",
        },
        {
          title: "负责人",
          dataIndex: "personId",
          render: (value, project) => (
            <span>
              {users.find((el) => el.id === project.personId)?.name || "none"}
            </span>
          ),
        },
        {
          title: "创建时间",
          dataIndex: "created",
          render: (value) => (
            <span>{value ? dayjs(value).format("YYYY-MM-DD") : "无"}</span>
          ),
        },
        {
          render: (value, project) => (
            <Dropdown
              overlay={
                <Menu>
                  <Menu.Item onClick={props.open}>{"编辑"}</Menu.Item>
                </Menu>
              }
            >
              <ButtonNoPadding style={{ border: "none" }}>
                {"..."}
              </ButtonNoPadding>
            </Dropdown>
          ),
        },
      ]}
      {...props}
    />
  );
};
