import { useDebounce } from "utils/index";
import { SearchPanel } from "./search-panel";
import { List } from "./list";
import { useUsers } from "utils/user";
import { useDocumentTitle } from "utils";
import { useProjectSearchParam } from "./utils";
import { useProjects } from "utils/project";
import { Row } from "components/lib";
import { Button } from "antd";

export interface User {
  id: number;
  name: string;
  token: string;
}
export interface Project {
  id: number;
  personId: number;
  name: string;
  pin: boolean;
  organization: string;
  created: number;
}

export const ProjectListScreen = (props: { open: () => void }) => {
  useDocumentTitle("项目列表", false);
  const [param, setParam] = useProjectSearchParam();
  const debounceParam = useDebounce(param, 500);
  // const client = useHttp();
  // const { run, isLoading, data: list} = useAsync<Project[]>();
  const { isLoading, data: list, retry } = useProjects(debounceParam);
  const { data: users } = useUsers();

  // useEffect(() => {
  // 封装useAsync
  // run(client('projects', { data: cleanObject(debounceParam) }));

  //封装useHttp
  // client('projects', {
  //     data: cleanObject(debounceParam)
  // })
  // .then(setList)
  // .catch(err => {
  //     setList([]);
  //     setError(err);
  // })
  // .finally(() => {
  //     setLoading(false);
  // });

  // fetch(`${apiUrl}/projects?${qs.stringify(cleanObject(debounceParam))}`).then(async res => {
  //     if(res.ok){
  //         setList(await res.json());
  //     }
  // });
  // eslint-disable-line react-hooks/exhaustive-deps
  // }, [debounceParam]);

  // useMount(() => {
  // client('users').then(setUsers);

  // fetch(`${apiUrl}/users`).then(async res => {
  //     if(res.ok){
  //         setUsers(await res.json());
  //     }
  // });
  // });

  return (
    <div>
      <Row between={true} marginBottom={2}>
        <SearchPanel users={users || []} param={param} setParam={setParam} />
        <Button type={"link"} onClick={props.open}>
          创建项目
        </Button>
      </Row>
      <List
        refresh={retry}
        loading={isLoading}
        users={users || []}
        dataSource={list || []}
        open={props.open}
      />
    </div>
  );
};
